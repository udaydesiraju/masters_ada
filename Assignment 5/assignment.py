# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 21:58:09 2020

@author: UdayD
"""

import numpy as np;
import pandas as pd;
import seaborn as sns;
import matplotlib.pyplot as plt;

# Making a list of missing value types
missing_values = ["n/a", "na", "--", "?"]

# Read data
main_df = pd.read_csv('Wisconsin Hospital Breast Cancer Data set 2016.csv', na_values=missing_values);

# 2 Data Understanding

print(main_df.head())

print(main_df.info())

print(main_df.describe())



# 3 Data Preparation

# check for null
print('\ncolumn wise null values')
print(main_df.isnull().sum());
print('\nTotal null values')
print(main_df.isnull().sum().sum());

# Drop all rows with na values
print('dropping all rows with null values')
main_df.dropna()

# Converting good/bad to boolean
main_df['diagnosis'] = main_df['diagnosis'].apply(lambda x: 1 if x == 'M' else 0)

# correlation matrix
df_t = pd.DataFrame(main_df.iloc[:, 1:len(main_df.columns)].values, columns=main_df.columns[1:len(main_df.columns)])
corr_mat = df_t.corr()

sns.heatmap(corr_mat, annot=True)

# selected columns
def feature_selection(data, corr):
    columns = np.full((corr.shape[0],), True, dtype=bool)
    for j in range(corr.shape[0]):
        if corr.iloc[0,j] < 0.1:
            if columns[j]:
                columns[j] = False


def feature_selection2(data, corr):
    columns = np.full((corr.shape[0],), True, dtype=bool)
    for i in range(corr.shape[0]):
        for j in range(i+1, corr.shape[0]):
            if corr.iloc[i,j] > 0.9:
                if columns[j]:
                    columns[j] = False
    selected_columns = data.columns[columns]
    return data[selected_columns]

hybrid_df = feature_selection2(df_t, corr_mat)

sns.heatmap(hybrid_df.corr(), annot=True)

# Shuffling and spliting data     
t_main_df = hybrid_df.sample(frac=1, random_state=108) 
X = t_main_df.iloc[:,1:len(hybrid_df.columns)].values
y = t_main_df.iloc[:, 0].values


# 4 Modelling
   
def m_train_validate_test_split(X, y, test_size=0.2, validation_size=0.2):
    X_train, X_validate, X_test, y_train, y_validate, y_test = [],[],[],[],[],[]
    train_size = 1 - (test_size+validation_size)
    test_size = 1-test_size;
    X_train, X_validate, X_test = np.split(X, [int(train_size*len(X)), int(test_size*len(X))])
    y_train, y_validate, y_test = np.split(y, [int(train_size*len(y)), int(test_size*len(y))])
    return np.asarray(X_train), np.asarray(X_validate), np.asarray(X_test), np.asarray(y_train), np.asarray(y_validate), np.asarray(y_test)
   
from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler()
X = scaler.fit_transform(X)

X_train, X_validate, X_test, y_train, y_validate, y_test = m_train_validate_test_split(X,y,test_size=0.2, validation_size=0.2);

from sklearn.metrics import roc_curve, auc
def plot_roc_curve(y_test, y_pred):
    fpr, tpr, thresholds = roc_curve(y_test, np.array(y_pred))
    print("AUC: ", auc(fpr, tpr))
    print("ROC curve")
    plt.title("ROC Curve")
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.plot(fpr, tpr)
    plt.plot([0, 1], ls="--")
    plt.plot([0, 0], [1, 0] , c=".7"), plt.plot([1, 1] , c=".7")
    plt.show()


## KNN
print("####################################")
print("K-Nearest Neighbours")
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

# Create classifier.
clf = KNeighborsClassifier()
clf.fit(X_train, y_train)

from matplotlib.colors import ListedColormap
def plotIt(X,y, classifier):
    X_set, y_set = X,y
    X1, X2 = np.meshgrid(np.arange(start = X_set[:, 0].min() - 1, stop = X_set[:, 0].max() + 1, step = 0.01), np.arange(start = X_set[:, 1].min() - 1, stop = X_set[:, 1].max() + 1, step = 0.01))
    plt.contourf(X1, X2, classifier.predict(np.array([X1.ravel(), X2.ravel()]).T).reshape(X1.shape),
                 alpha = 0.75, cmap = ListedColormap(('red', 'green')))
    plt.xlim(X1.min(), X1.max())
    plt.ylim(X2.min(), X2.max())
    for i, j in enumerate(np.unique(y_set)):
        plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, 1],
                    c = ListedColormap(('red', 'green'))(i), label = j)
    plt.title('K-NN (Test set)')
    plt.xlabel('Age')
    plt.ylabel('Estimated Salary')
    plt.legend()
    plt.show()

# Predicting the Test set results
y_pred = clf.predict(X_train)
print(confusion_matrix(y_train, y_pred))
print(classification_report(y_train, y_pred))

y_pred = clf.predict(X_validate)
print(confusion_matrix(y_validate, y_pred))
print(classification_report(y_validate, y_pred))
plot_roc_curve(y_validate, y_pred) 

y_pred = clf.predict(X_test)
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))
#plotIt(X_test, y_test, clf)

plot_roc_curve(y_test, y_pred) 


## Random Forest Classifier

print("####################################")
print("Random Forest")
from sklearn.ensemble import RandomForestClassifier
model=RandomForestClassifier(n_estimators=100)
model.fit(X_train,y_train)


# Predicting the Test set results
y_pred = clf.predict(X_train)
print(confusion_matrix(y_train, y_pred))
print(classification_report(y_train, y_pred))

y_pred = clf.predict(X_validate)
print(confusion_matrix(y_validate, y_pred))
print(classification_report(y_validate, y_pred))
plot_roc_curve(y_validate, y_pred) 

y_pred = clf.predict(X_test)
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))

plot_roc_curve(y_test, y_pred) 

# Artificial Neural Network
print("####################################")
print("ANN")
import keras
from keras.models import Sequential
from keras.layers import Dense

# Initialising the ANN
classifier = Sequential()

# Adding the input layer and the first hidden layer
classifier.add(Dense(output_dim = 4, init = 'uniform', activation = 'relu', input_dim = (X_train.shape[1])))

# Adding the second hidden layer
classifier.add(Dense(output_dim = 4, init = 'uniform', activation = 'relu'))
#
# Adding the third hidden layer
#classifier.add(Dense(output_dim = 8, init = 'uniform', activation = 'relu'))

# Adding the output layer
classifier.add(Dense(output_dim = 1, init = 'uniform', activation = 'sigmoid'))

# Compiling the ANN
classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

# Fitting the ANN to the Training set
classifier.fit(X_train, y_train, batch_size = 10, nb_epoch = 100)

# Predicting the Test set results
y_pred = classifier.predict(X_train)
y_pred = (y_pred > 0.5)
print(confusion_matrix(y_train, y_pred))
print(classification_report(y_train, y_pred))

y_pred = classifier.predict(X_validate)
y_pred = (y_pred > 0.5)
print(confusion_matrix(y_validate, y_pred))
print(classification_report(y_validate, y_pred))

y_pred = classifier.predict(X_test)
y_pred = (y_pred > 0.5)
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))

plot_roc_curve(y_test, y_pred) 
#from ann_visualizer.visualize import ann_viz;
##Build your model here
#ann_viz(classifier)

"""
import statsmodels.api as sm
from scipy import stats
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
X = main_df.iloc[:,2:len(main_df.columns)].values
y = main_df.iloc[:, 1].values
X2 = sm.add_constant(X)
est = sm.Logit(y,X)
est2 = est.fit()
print(est2.summary())

from sklearn.feature_selection import chi2
scores, pvalues = chi2(X, y)

X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.2, random_state=101)
#create an instance and fit the model 
logmodel = LogisticRegression()
y_pred = logmodel.fit(X_train, y_train)

from sklearn.feature_selection import chi2
scores, pvalues = chi2(X, y)

[print("%.2f" % (x)) for x in pvalues]
# Descriptive Statistics

## Univariate

### Location statistics
def locationStats(x):
    print('\n####### Location Statistics #######')
    for col in x.columns:        
        if str(x[col].dtype) == 'object':            
            print(x.groupby(col)[col].agg(frequency='count'))
            print('mode  %s' % x[col].describe()['top'])
        else:
            print('\n', col)
            print(x[col].describe())
    

### Dispersion statistics
def dispersionStats(x):
    print('\n####### Dispersion Statistics #######')
    for col in x.columns:
        print('\n', col)
        if str(x[col].dtype) != 'object':
            desc = x[col].describe()
            print('Interquartile range: ', desc['50%'] - desc['25%'])
            print('Amplitude: ', desc['max'] - desc['min'])
            print('Mean Absolute Deviation: ', x[col].mad())
        else:
            print('No dispersion statistics available for this variable')


locationStats(main_df)        
dispersionStats(main_df)



# main_df.groupby('age').sum().plot(kind='bar')
def data_describe(x, col):
    sub_df = x.groupby(col)[col].agg(frequency='count')
    print(sub_df)
    sub_df.plot(kind='bar', y='frequency')
    

# Age
data_describe(main_df, 'age')
print('\nwomen of [50-59] age group has highest chances of breast cancer')
# Menopause
data_describe(main_df, 'menopause')
print('\nwomen with premeanopause has highest chances of breast cancer')

"""