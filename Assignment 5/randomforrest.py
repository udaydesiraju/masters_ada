

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix,accuracy_score





df = pd.read_csv("breast_cancer.csv")

df['diagnosis'] = (df['diagnosis'] == 'M').astype(int) #converting M&B into 1 and 0
df['diagnosis']




from sklearn.model_selection import train_test_split
df.keys()




y=df.iloc[:,1] 
X=df.iloc[:,2:32]



#training size = 40% and test size = 40% and Validation size = 20%
def m_train_validate_test_split(X, y, test_size=0.4, validation_size=0.2):
    X_train, X_validate, X_test, y_train, y_validate, y_test = [],[],[],[],[],[]    
    X_train, X_validate, X_test = np.split(X, [int(.6*len(X)), int(.8*len(X))])
    y_train, y_validate, y_test = np.split(y, [int(.6*len(y)), int(.8*len(y))])
    return np.asarray(X_train), np.asarray(X_validate), np.asarray(X_test), np.asarray(y_train), np.asarray(y_validate), np.asarray(y_test)




from sklearn.ensemble import RandomForestClassifier
X_train, X_validate, X_test, y_train, y_validate, y_test = m_train_validate_test_split(X,y)

model=RandomForestClassifier(n_estimators=100)
model.fit(X_train,y_train)
yy_validate=model.predict(X_validate)
y_pred=model.predict(X_test)

yy_train=model.predict(X_train)





confusion_matrix(y_test,y_pred)
confusion_matrix(y_validate,yy_validate)
#confusion_matrix(y_train,yy_train)






print("Accuracy:",accuracy_score(y_test, y_pred))

print("Accuracy:",accuracy_score(y_validate, yy_validate))
print("Accuracy:",accuracy_score(y_train, yy_train))


# In[9]:


estimator = model.estimators_[10]


# In[10]:


from sklearn.tree import export_graphviz
from sklearn import tree


# In[11]:


cn=["M","B"] #target_names
fn=df.iloc[0,2:32] #feature_names
fig, axes = plt.subplots(nrows = 1,ncols = 1,figsize = (4,4), dpi=800)
tree.plot_tree(estimator,
               feature_names = fn, 
               class_names=cn);
fig.savefig('rf_individualtree.png')


# In[ ]:




