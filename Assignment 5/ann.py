# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 17:06:22 2020

@author: UdayD
"""

import numpy as np;
import random as rd;
import matplotlib.pyplot as plt;

#sigmoid function
def sigmoid(x):
    return 1.0/(1.0 + np.exp(-x))

def sigmoid_prime(x):
    return sigmoid(x)*(1.0-sigmoid(x))

#ReLu function
def relu(X):
   return np.maximum(0,X)

def relu_prime(X):
    return 0 if X<0 else 1;


# Initialize a network
def initialize_network(n_inputs, n_hidden, n_outputs):
    print("Preparing architecture for [%d-%d-%d]" % (n_inputs, n_hidden, n_outputs))
    network = list();
    hidden_layer = [{'weights':[rd.random() for i in range(n_inputs + 1)]} for i in range(n_hidden)]
    network.append(hidden_layer);
    output_layer = [{'weights':[rd.random() for i in range(n_hidden + 1)]} for i in range(n_outputs)]
    network.append(output_layer);
    return network

# Calculate neuron activation for an input
def activate(weights, inputs):
#   adding bias
	activation = weights[-1]
#    dot product
	for i in range(len(weights)-1):
		activation += weights[i] * inputs[i]
	return activation

def feedforward(network, row):
    inputs=row;
    for i in range(len(network)):
        layer = network[i]
        new_inputs = []
        for neuron in layer:
            activation = activate(neuron["weights"], inputs)
            neuron["output"] = sigmoid(activation) if i == (len(network)-1) else relu(activation)
            new_inputs.append(neuron['output'])
        inputs = new_inputs
    return inputs;

def backpropogation(network, expected):
	for i in reversed(range(len(network))):
		layer = network[i]
		errors = list()
		if i != len(network)-1:
			for j in range(len(layer)):
				error = 0.0
				for neuron in network[i + 1]:
					error += (neuron['weights'][j] * neuron['delta'])
				errors.append(error)
		else:
			for j in range(len(layer)):
				neuron = layer[j]
				errors.append(expected[j] - neuron['output'])
		for j in range(len(layer)):
			neuron = layer[j]
			neuron['delta'] = errors[j] * (sigmoid_prime(neuron['output']) if i==(len(network)-1) else relu_prime(neuron['output']))


# Update network weights with error
def update_weights(network, row, l_rate):
	for i in range(len(network)):
		inputs = row[:-1]
		if i != 0:
			inputs = [neuron['output'] for neuron in network[i - 1]]
		for neuron in network[i]:
			for j in range(len(inputs)):
				neuron['weights'][j] += l_rate * neuron['delta'] * inputs[j]
			neuron['weights'][-1] += l_rate * neuron['delta']


def train_network(network, train, l_rate, n_outputs,  n_epoch=100, trace_log=True):
    for epoch in range(0,n_epoch):
        sum_error = 0
        for row in train:
            outputs = feedforward(network, row)
            expected = [0 for i in range(n_outputs)]
            expected[int(row[-1])] = 1
            sum_error += sum([(expected[i]-outputs[i])**2 for i in range(len(expected))])
            backpropogation(network, expected)
            update_weights(network, row, l_rate)
        if(trace_log and epoch%5==0):
            print('>epoch=%d, lrate=%.3f, error=%.3f' % (epoch, l_rate, sum_error))
    return outputs
        

# Prepare dataset
def prepare_ds(X, y):
    # Feature Scaling
    from sklearn.preprocessing import StandardScaler
    sc = StandardScaler()
    X = sc.fit_transform(X)    
    return np.column_stack((X, y));

def cross_entropy(yHat, y):
    if (y==1 and yHat==0): 
        return 0;
    
    if (y==0 and yHat==1):
        return 0;
    
    if yHat>1:
        print(yHat, y)
    
    if y == 1:
      return -y*np.log(yHat)
    else:
      return -(1-y)*np.log(1 - yHat)
  
def res_color(y, metric="binary"):
    color_arr=[];
    for x in y:
        if(metric=="binary"):
            color_arr.append("green" if x==1 else "red")
        else:
            color_arr.append("green" if np.argmax(x)==1 else "red")
    return color_arr
    
    
def plot(X, color_arr, label):
    plt.title(label)
    plt.xlabel("y_actual")
    plt.ylabel("y_pred")
    plt.scatter(X[:,0], X[:,1], c=color_arr)
    plt.show()
    

def plot_roc_curve(y_test, y_pred):
    fpr, tpr, thresholds = roc_curve(y_test, np.array(y_pred))
    print("AUC: ", auc(fpr, tpr))
    print("ROC curve")
    plt.title("ROC Curve")
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.plot(fpr, tpr)
    plt.plot([0, 1], ls="--")
    plt.plot([0, 0], [1, 0] , c=".7"), plt.plot([1, 1] , c=".7")
    plt.show()


# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
def conf_mat(X, y):
    return confusion_matrix(X[:,-1], y)
    
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import classification_report
from sklearn.metrics import roc_curve, auc
def log_results(X, y, network, final_res, n_count=15, has_plt=True):
    pred_arr=[]
    print("##########################")
    print("Network Details", network)    
    print("##########################")
    print("Logging Predictions for %d observations" % n_count)
    for row in X:
        ce = 0
        pred = feedforward(network, row)
        pred_arr.append(pred) # [1 if row[-1]==1 else 0]
        ce += cross_entropy(pred[1 if row[-1]==1 else 0], row[-1]) 
    for i in range(n_count):
        print(row[-1], pred)        
    print("##########################")
    print("Cross Entropy Loss: %.3f " % (ce/len(X)))
    print("##########################")
    pred_val = []
    for x in pred_arr:
        pred_val.append(1 if np.argmax(x)==1 else 0)
    cm = conf_mat(X, pred_val)
    print("Confusion Matrix: ", cm)
    print("##########################")
    # precision tp / (tp + fp)
    precision = precision_score(y, pred_val)
    print('Precision: %f' % precision)
    # recall: tp / (tp + fn)
    recall = recall_score(y, pred_val)
    print('Recall: %f' % recall)
    print("##########################")
    print("Accuracy: ", (cm[0,0] + cm[1,1])/cm.sum())
    print("##########################")
    print("Classification Report: \n", classification_report(y, pred_val))
    print("##########################")
    plot_roc_curve(y, np.array(pred_val))    
    if(has_plt):
        color_arr=res_color(X[:,-1]);        
        plot(X, color_arr, "Actual (y)")   
        color_arr=res_color(pred_arr, "max");   
        plot(X, color_arr, "Predicted (yHat)")
    
def m_train_validate_test_split(X, y, test_size=0.2, validation_size=0.2):
    X_train, X_validate, X_test, y_train, y_validate, y_test = [],[],[],[],[],[]
    train_size = 1 - (test_size+validation_size)
    test_size = 1-test_size;
    X_train, X_validate, X_test = np.split(X, [int(train_size*len(X)), int(test_size*len(X))])
    y_train, y_validate, y_test = np.split(y, [int(train_size*len(y)), int(test_size*len(y))])
    return np.asarray(X_train), np.asarray(X_validate), np.asarray(X_test), np.asarray(y_train), np.asarray(y_validate), np.asarray(y_test)
    