# -*- coding: utf-8 -*-
"""
Created on Wed Feb  5 16:52:15 2020

@author: UdayD
"""

# Import varaibles
import numpy as np;
import pandas as pd;
import matplotlib.pyplot as plt;

# Read Data File
main_df = pd.read_csv('asnmt_social_network.csv');

# Understanding data
print(main_df.head(3))
print(main_df.describe())

plt.rcParams['figure.figsize'] = (10,6)

"""

Q1) Using the social network dataset, run the k-means algorithm for different values of k (k = 2, k = 3 and k = 5). 

"""
# [1,2] are the indexes of the columns which we want to process for kmeans
X = main_df.iloc[:, [1,2]].values

# Using the elbow method to find the optimal number of clusters
from sklearn.cluster import KMeans
wcss=[]
for i in range(1, 11):
    kmeans = KMeans(n_clusters=i, init='k-means++', max_iter=300, n_init=10, random_state=10)
    kmeans.fit(X)
    wcss.append(kmeans.inertia_)

plt.plot(range(1,11), wcss)
plt.title('The Elbow Method')
plt.xlabel('Number of clusters')
plt.ylabel('WCSS')
plt.show()

# Applying k-means to the social network dataset
def getKMeans(Y, n_clusters):   

    kmeans = KMeans(n_clusters=n_clusters, init='k-means++', max_iter=300, n_init=10, random_state=10)
    y_hc = kmeans.fit_predict(Y)
    
    colors = np.array(['red','blue','green','orange','magenta'])
    
    # Visualising clusters
    for i in range(0, n_clusters):
        plt.scatter(Y[y_hc==i, 0], Y[y_hc==i,1], s=100, c=colors[i], label='Cluster ' + str(i+1))
        
#    plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:,1], s=300, c='yellow', label='Centroids')
    plt.title('Cluster of Users')
    plt.xlabel('Age')
    plt.ylabel('Educational Level')
    plt.legend()
    plt.show()


getKMeans(X, n_clusters=2)
getKMeans(X, n_clusters=3)
getKMeans(X, n_clusters=5)




"""

Q2) Using the social network dataset, run the DBSCAN algorithm and test different values for the two main hyper-parameters.
    
"""

from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler
from sklearn import metrics
from sklearn.neighbors import NearestNeighbors


# Scaling the data to bring all the attributes to a comparable level 
scaler = StandardScaler() 
X_scaled = scaler.fit_transform(X) 

neigh = NearestNeighbors(n_neighbors=2)
nbrs = neigh.fit(X_scaled)
distances, indices = nbrs.kneighbors(X_scaled)

distances = np.sort(distances, axis=0)
distances = distances[:,1]
plt.title('Distances')
plt.plot(distances)
plt.show()

def getDBSCAN(eps_, min_samples_):
    dbscan = DBSCAN(eps=eps_, min_samples=min_samples_).fit(X_scaled)
    labels = dbscan.labels_
    
    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)
    
    colors = ['royalblue', 'maroon', 'forestgreen', 'mediumorchid', 'tan', 'deeppink', 'olive', 'goldenrod', 'lightcyan', 'navy']
        
    # Visualising clusters
    for i in range(0, n_clusters_):
            plt.scatter(X_scaled[:, 0], X_scaled[:,1], s=100, c=colors[i], label='Cluster ' + str(i+1))
            
    plt.title('Cluster of Users')
    #plt.xlabel('Age')
    #plt.ylabel('Educational Level')
    plt.show()
    
    print('Estimated number of clusters: %d' % n_clusters_)
    print('Estimated number of noise points: %d' % n_noise_)
    print("Silhouette Coefficient: %0.3f" % metrics.silhouette_score(X_scaled, labels))
    
getDBSCAN(1.125, 3)
#getDBSCAN(1.25, 3)
#getDBSCAN(1.125, 10)
#getDBSCAN(5, 2)


"""

Q3) Using the social network dataset, run the agglomerative hierarchical algorithm and plot the dendrogram generated.

"""

from scipy.cluster import hierarchy as sch
dendrogram = sch.dendrogram(sch.linkage(X, method='ward'))
# ward method minimizes variamce wrt each cluster

plt.title('Dendrogram')
plt.xlabel('Users')
plt.ylabel('Euclidian Distances')
plt.show()

def getAgglomerative(n_clusters_):
    # Fitting hierarchial clustering to mall dataset
    from sklearn.cluster import AgglomerativeClustering
    hc = AgglomerativeClustering(n_clusters= n_clusters_, affinity='euclidean', linkage='ward')
    y_hc= hc.fit_predict(X)
    
    colors = np.array(['red','blue','green','orange','magenta'])
        
    # Visualising clusters
    for i in range(0, n_clusters_):
        plt.scatter(X[y_hc==i, 0], X[y_hc==i,1], s=100, c=colors[i], label='Cluster ' + str(i+1))
    
    plt.title('Cluster of Users')
    plt.xlabel('Age')
    plt.ylabel('Educational Level')
    plt.legend()
    plt.show()
    
# From the dendrogram, by drawing the horizontal line to seperate clusters, we get 3 clusters
getAgglomerative(3)


"""

Q4) What happens if you change the order in which you present the data to the k-means algorithm? Use the social network dataset to find out. 
    
"""
tmain_df = main_df.sort_values('Age')
X1 = tmain_df.iloc[:, [1,2]].values

getKMeans(X1, 2)
getKMeans(X1, 3)
getKMeans(X1, 5)

# Clusters changed when compared 