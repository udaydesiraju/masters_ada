# -*- coding: utf-8 -*-
"""
Created on Sat Mar  7 23:56:30 2020

@author: UdayD
"""

import numpy as np;
import matplotlib.pyplot as plt
import pandas as pd

main_df = pd.read_csv('social_ds.csv')

main_df.describe()

# Converting good/bad to boolean
main_df['Company'] = main_df['Company'].apply(lambda x: 1 if x == 'Good' else 0)

X = main_df.iloc[:, [1,2]].values
y = main_df.iloc[:, 3].values

def m_train_test_split(X, y, test_size=0.3):
    X_train, X_test, y_train, y_test = [],[],[],[]
    train_size=int(len(X)*(1-test_size))
    test_size=int(len(X)*(test_size))
    for i in range(train_size):
        X_train.append(X[i,:])
        y_train.append(y[i])
    for i in range(test_size):
        X_test.append(X[i,:])
        y_test.append(y[i])
    return np.asarray(X_train), np.asarray(X_test), np.asarray(y_train), np.asarray(y_test)
    

def knn(k=3, m_test_size=0.25, seed=1):
    # Splitting the dataset into the Training set and Test set
    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = m_train_test_split(X, y,test_size=m_test_size)
    print(X_train, X_test, y_train, y_test )
    
    # Feature Scaling
    from sklearn.preprocessing import StandardScaler
    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)
    
    # Fitting K-NN to the Training set
    from sklearn.neighbors import KNeighborsClassifier
    #classifier = KNeighborsClassifier(n_neighbors = 5, metric = 'minkowski', p = 2)
    classifier = KNeighborsClassifier(n_neighbors = k, metric = 'euclidean')
    #classifier = KNeighborsClassifier(n_neighbors = 7, metric = 'euclidean')
    classifier.fit(X_train, y_train)
    
    # Predicting the Test set results
    y_pred = classifier.predict(X_test)
    
    # Making the Confusion Matrix
    from sklearn.metrics import confusion_matrix
    cm = confusion_matrix(y_test, y_pred)
    
    # Visualising the Training set results
    from matplotlib.colors import ListedColormap
    X_set, y_set = X_train, y_train
    X1, X2 = np.meshgrid(np.arange(start = X_set[:, 0].min() - 1, stop = X_set[:, 0].max() + 1, step = 0.01),
                         np.arange(start = X_set[:, 1].min() - 1, stop = X_set[:, 1].max() + 1, step = 0.01))
    plt.contourf(X1, X2, classifier.predict(np.array([X1.ravel(), X2.ravel()]).T).reshape(X1.shape),
                 alpha = 0.75, cmap = ListedColormap(('red', 'green')))
    plt.xlim(X1.min(), X1.max())
    plt.ylim(X2.min(), X2.max())
    for i, j in enumerate(np.unique(y_set)):
        plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, 1],
                    c = ListedColormap(('red', 'green'))(i), label = j)
    plt.title('K-NN (Training set)')
    plt.xlabel('Age')
    plt.ylabel('Education Level')
    plt.legend()
    plt.show()
    plt.savefig("train.png")
    
    # Visualising the Test set results
    from matplotlib.colors import ListedColormap
    X_set, y_set = X_test, y_test
    X1, X2 = np.meshgrid(np.arange(start = X_set[:, 0].min() - 1, stop = X_set[:, 0].max() + 1, step = 0.01),
                         np.arange(start = X_set[:, 1].min() - 1, stop = X_set[:, 1].max() + 1, step = 0.01))
    plt.contourf(X1, X2, classifier.predict(np.array([X1.ravel(), X2.ravel()]).T).reshape(X1.shape),
                 alpha = 0.75, cmap = ListedColormap(('red', 'green')))
    plt.xlim(X1.min(), X1.max())
    plt.ylim(X2.min(), X2.max())
    for i, j in enumerate(np.unique(y_set)):
        plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, 1],
                    c = ListedColormap(('red', 'green'))(i), label = j)
    plt.title('K-NN (Test set)')
    plt.xlabel('Age')
    plt.ylabel('Education Level')
    plt.legend()
    plt.show()
    plt.savefig("test.png")
    print("Accuracy: ", (cm[0,0] + cm[1,1])/cm.sum())

print("#############k=2#################")
knn(2,0.5, 1)
print("#############k=3#################")
knn(3,0.25, 1)
print("#############k=5#################")
knn(5,0.25, 1)

print("##############################")
knn(3,0.3, 100)
print("##############################")
knn(5,0.45, 250)