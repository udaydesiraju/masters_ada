# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 02:26:13 2020

@author: adity
"""
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from mlxtend.plotting import plot_decision_regions

from sklearn import metrics
pima = pd.read_csv('social_ds.csv')
pima['Company'] = pima['Company'].apply(lambda x: 1 if x == 'Good' else 0)

X = pima.iloc[:, [1,2]].values
y = pima.iloc[:, 3].values

print("Features:",X)
print("Labels:",y)


pima.shape

def m_train_test_split(X, y, test_size=0.3):
    X_train, X_test, y_train, y_test = [],[],[],[]
    train_size=int(len(X)*(1-test_size))
    test_size=int(len(X)*(test_size))
    for i in range(train_size):
        X_train.append(list(X[i,:]))
        y_train.append(y[i])
    for i in range(test_size):
        X_test.append(list(X[i,:]))
        y_test.append(y[i])
    return np.asarray(X_train), np.asarray(X_test), np.asarray(y_train), np.asarray(y_test)
#X_train.tolist(), X_test.tolist(), y_train.tolist(), y_test.tolist()

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = m_train_test_split(X, y, test_size=0.5) # 5,random_state=50)

from sklearn import svm

#Create a svm Classifier
def create_svm(m_kernal='rbf', m_gamma='scale', m_C=1.0, m_degree=3):
    #clf = svm.SVC(kernel='poly',degree=2)
    #clf = svm.SVC(kernel='linear')# Linear Kernel
    
    clf = svm.SVC(kernel=m_kernal,gamma=m_gamma,C=m_C, degree=m_degree)
    
    #Train the model using the training sets
    clf.fit(np.array(X_train), y_train)
    
    #Predict the response for test dataset
    y_pred = clf.predict(X_test)
    
    print("Accuracy:",metrics.accuracy_score(y_test, y_pred))
    
    
    plot_decision_regions(X=X_train, 
                          y=y_train,
                          clf=clf, 
                          legend=2)
    
    plt.xlabel('Age', size=14)
    plt.ylabel('Education level', size=14)
    plt.title('SVM Decision Region Boundary', size=16)


create_svm('rbf', m_gamma=.01, m_C=1)
create_svm('poly', m_degree=2)
create_svm('linear')
