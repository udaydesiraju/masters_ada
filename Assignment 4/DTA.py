# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 02:16:04 2020

@author: adity
"""

import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree 
from sklearn.model_selection import train_test_split 

main_df = pd.read_csv('social_ds.csv')

main_df.describe()

main_df.head(21)

feature_cols = ['Age','Education level']
X = main_df[feature_cols] # Features
y = main_df.Company

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5, random_state=1)

# Create Decision Tree classifer object
classifier = DecisionTreeClassifier()

# Train Decision Tree Classifer
classifier = classifier.fit(X_train,y_train)

#Predict the response for test dataset
y_pred = classifier.predict(X_test)


# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)

# Visualising the Training set results
tree.plot_tree(classifier)
tree.export_graphviz(classifier, class_names=True)

print("Accuracy: ", (cm[0,0] + cm[1,1])/cm.sum())
