# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 13:46:52 2020

@author: UdayD
"""

import numpy as np;
import random as rd;
import matplotlib.pyplot as plt;

def sigmoid(x):
    return 1.0/(1.0 + np.exp(-x))

def sigmoid_prime(x):
    return sigmoid(x)*(1.0-sigmoid(x))

def tanh(x):
    return np.tanh(x)

def tanh_prime(x):
    return 1.0 - x**2

# Initialize a network
def initialize_network(n_inputs, n_hidden, n_outputs):
    print("Preparing architecture for [%d-%d-%d]" % (n_inputs, n_hidden, n_outputs))
    network = list();
    hidden_layer = [{'weights':[rd.random() for i in range(n_inputs + 1)]} for i in range(n_hidden)]
    network.append(hidden_layer);
    output_layer = [{'weights':[rd.random() for i in range(n_hidden + 1)]} for i in range(n_outputs)]
    network.append(output_layer);
    return network

# Calculate neuron activation for an input
def activate(weights, inputs):
#   adding bias
	activation = weights[-1]
#    dot product
	for i in range(len(weights)-1):
		activation += weights[i] * inputs[i]
	return activation

def feedforward(network, row):
    inputs=row;
    for layer in network:
        new_inputs = []
        for neuron in layer:
            activation = activate(neuron["weights"], inputs)
            neuron["output"] = sigmoid(activation)
            new_inputs.append(neuron['output'])
        inputs = new_inputs
    return inputs;

def backpropogation(network, expected):
	for i in reversed(range(len(network))):
		layer = network[i]
		errors = list()
		if i != len(network)-1:
			for j in range(len(layer)):
				error = 0.0
				for neuron in network[i + 1]:
					error += (neuron['weights'][j] * neuron['delta'])
				errors.append(error)
		else:
			for j in range(len(layer)):
				neuron = layer[j]
				errors.append(expected[j] - neuron['output'])
		for j in range(len(layer)):
			neuron = layer[j]
			neuron['delta'] = errors[j] * sigmoid_prime(neuron['output'])


# Update network weights with error
def update_weights(network, row, l_rate):
	for i in range(len(network)):
		inputs = row[:-1]
		if i != 0:
			inputs = [neuron['output'] for neuron in network[i - 1]]
		for neuron in network[i]:
			for j in range(len(inputs)):
				neuron['weights'][j] += l_rate * neuron['delta'] * inputs[j]
			neuron['weights'][-1] += l_rate * neuron['delta']


def train_network(network, train, l_rate, n_outputs,  n_epoch=10000, trace_log=True):
    for epoch in range(0,n_epoch):
        sum_error = 0
        for row in train:
            outputs = feedforward(network, row)
            expected = [0 for i in range(n_outputs)]
            expected[int(row[-1])] = 1
            sum_error += sum([(expected[i]-outputs[i])**2 for i in range(len(expected))])
            backpropogation(network, expected)
            update_weights(network, row, l_rate)
        if(trace_log):
            print('>epoch=%d, lrate=%.3f, error=%.3f' % (epoch, l_rate, sum_error))
    return outputs
        

# Prepare dataset
def prepare_ds(X, y):
    # Feature Scaling
    from sklearn.preprocessing import StandardScaler
    sc = StandardScaler()
    X = sc.fit_transform(X)    
    return np.column_stack((X, y));

def cross_entropy(yHat, y):
    if y == 1:
      return -y*np.log(yHat)
    else:
      return -(1-y)*np.log(1 - yHat)
  
def res_color(y, metric="binary"):
    color_arr=[];
    for x in y:
        if(metric=="binary"):
            color_arr.append("green" if x==1 else "red")
        else:
            color_arr.append("green" if np.argmax(x)==1 else "red")
    return color_arr
    
    
def plot(X, color_arr, label):
    plt.title(label)
    plt.xlabel("Age")
    plt.ylabel("Education level")
    plt.scatter(X[:,0], X[:,1], c=color_arr)
    plt.show()

# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
def conf_mat(X, y):
    return confusion_matrix(X[:,-1], y)
    

def log_results(X, network, final_res, plt=True):
    pred_arr=[]
    print("##########################")
    print("Network Details", network)
    
    print("##########################")
    print("Logging Predictions")
    for row in X:
        ce = 0
        pred = feedforward(network, row)
        print(row[-1], pred)
        pred_arr.append(pred) # [1 if row[-1]==1 else 0]
        ce += cross_entropy(pred[1 if row[-1]==1 else 0], row[-1]) 
    print("##########################")
    print("Cross Entropy Loss: %.3f " % (ce/len(X)))
    print("##########################")
    pred_val = []
    for x in pred_arr:
        pred_val.append(1 if np.argmax(x)==1 else 0)
    cm = conf_mat(X, pred_val)
    print("Confusion Matrix: ", cm)
    print("##########################")
    print("Accuracy: ", (cm[0,0] + cm[1,1])/cm.sum())
    if(plt):
        color_arr=res_color(X[:,-1]);        
        plot(X, color_arr, "Actual (y)")   
        color_arr=res_color(pred_arr, "max");   
        plot(X, color_arr, "Predicted (yHat)")
    

def m_train_test_split(X, y, test_size=0.3):
    X_train, X_test, y_train, y_test = [],[],[],[]
    train_size=int(len(X)*(1-test_size))
    test_size=int(len(X)*(test_size))
    for i in range(train_size):
        X_train.append(X[i,:])
        y_train.append(y[i])
    for i in range(test_size):
        X_test.append(X[i,:])
        y_test.append(y[i])
    return np.asarray(X_train), np.asarray(X_test), np.asarray(y_train), np.asarray(y_test)
    

# 
import pandas as pd
main_df = pd.read_csv('social_ds.csv')

main_df.describe()

# Converting good/bad to boolean
main_df['Company'] = main_df['Company'].apply(lambda x: 1 if x == 'Good' else 0)

X = main_df.iloc[:, [1,2]].values
y = main_df.iloc[:, 3].values

X_train, X_test, y_train, y_test = m_train_test_split(X,y,0.3);

X_ds = prepare_ds(X_train, y_train)

n_inputs = len(X_ds[0]) - 1
n_outputs = len(set([row[-1] for row in X_ds]))

network = initialize_network(n_inputs, 3, n_outputs)
res = train_network(network, X_ds, 0.5, n_outputs, 10000, False)
X_pred_ds = prepare_ds(X_test, y_test)
log_results(X_pred_ds, network, res)

network = initialize_network(n_inputs, 3, n_outputs)
res = train_network(network, X_ds, 0.02, n_outputs, 20000, False)
X_pred_ds = prepare_ds(X_test, y_test)
log_results(X_pred_ds, network, res)

network = initialize_network(n_inputs, 4, n_outputs)
res = train_network(network, X_ds, 0.01, n_outputs, 20000, False)
X_pred_ds = prepare_ds(X_test, y_test)
log_results(X_pred_ds, network, res)

network = initialize_network(n_inputs, 5, n_outputs)
res = train_network(network, X_ds, 0.01, n_outputs, 20000, False)
X_pred_ds = prepare_ds(X_test, y_test)
log_results(X_pred_ds, network, res)

